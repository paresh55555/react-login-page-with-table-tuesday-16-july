import React, { Component } from 'react';
import './dummy.css';
import { Button } from 'reactstrap';
import { Table } from 'reactstrap';
import '@fortawesome/react-fontawesome';
import 'react-fontawesome';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        username: "",
        email:"",
        number: "",
        items:[]
    }
  }

  handleChange =(e)=>{
      const {name, value} = e.target;
      this.setState({
          [name] : value
      });

      // let items = [...this.state.items];
      // items.push({username:this.state.username,email: this.state.email, number: this.state.number});
      console.log('username...',this.state.username);

  };

    validation = (e)=> {
        if(this.state.username === '' ){
            alert('Enter the username');
            return false;
        }if(this.state.email === ''){
            alert('Enter the email ID');
            return false;
        }if(this.state.number === ''){
            alert('Enter the number');
            return false;
        }else{
            const { items } = this.state;
            items.push({username:this.state.username,email: this.state.email, number: this.state.number});
            this.setState({
                items:items,
            });
        }


    };
        render() {
            const {items}= this.state;
            return (
                <div>
                    <div className ='box'>

                        <h1 className='title center'>Add Data</h1>&nbsp;
                        <label ><i className="fa fa-search"></i>User Name</label>
                        <input type="text" name='username' onChange={(e)=>{this.handleChange(e)}} className= 'form-control'  /> <br/>
                        <label>Email ID</label>
                        <input type="email" name='email' onChange={(e)=>{this.handleChange(e)}} className='form-control'/><br/>
                        <label >Contact No.</label>
                        <input type="text" name= 'number' onChange={(e)=>{this.handleChange(e)}} className='form-control' value={this.props.pass}/><br/>
                        <Button color='primary' onClick={this.validation} >submit</Button>
                    </div>
                    <div className='tableBox'>
                        <h2 className='center'>Data Table</h2>
                        <Table bordered  className ='tableData'>
                            <thead>
                                <th>username</th>
                                <th>email</th>
                                <th>contact</th>
                            </thead>
                            <tbody>
                            {items.map(d1 =>{
                                return(
                                    <tr>
                                        <td>{d1.username}</td>
                                        <td>{d1.email}</td>
                                        <td>{d1.number}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </Table>
                    </div>

                </div>
            );
        }
}
// class JTP extends React.Component {
//   render() {
//     return (
//         <div className ='box'>
//           <h1 className='title center'>log in</h1>&nbsp;
//           <label >USER NAME</label>
//           <input   type="text" name='username' onChange={(e)=>{}} className= 'form-control'  /> <br/>
//           <label >Password</label>
//           <input type="password" className='form-control' name="password" value={this.props.pass}/><br/>
//           <button className= 'btn btn-primary' onClick={this.validation} >submit</button>
//         </div>
//     );
//   }
// }
export default App;